import React, * as react from 'react';
import { Layout, Menu, Icon } from 'antd';
// import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';
import sidebarRoute from './../../../routes/sidebar';
import { connect } from 'react-redux';
const { Sider } = Layout;
const { SubMenu } = Menu;
class Aside extends react.Component {
  render() {
    return (
      <Sider width={256} trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo-themes" />
        <React.Fragment>
          <Menu
            theme="dark"
            mode="inline"
            className=""
            // eslint-disable-next-line react/prop-types
            defaultSelectedKeys={this.props.location.pathname}
            subMenuOpenDelay={0.5}
          >
            {sidebarRoute.map((prop, key) => {
              if (prop.children && prop.children.length > 0) {
                return (
                  <SubMenu
                    key={key}
                    title={
                      <span>
                        <Icon type={prop.icon} />
                        <span>{prop.name}</span>
                      </span>
                    }
                  >
                    {prop.children.map((subItem, keyItem) => {
                      return (
                        <Menu.Item key={subItem.path}>
                          <NavLink to={subItem.path} key={keyItem}>
                            <Icon type={subItem.icon} />
                            {subItem.name}
                          </NavLink>
                        </Menu.Item>
                      );
                    })}
                  </SubMenu>
                );
              } else {
                return (
                  <Menu.Item key={prop.path}>
                    <NavLink to={prop.path} key={key}>
                      <Icon type={prop.icon} />
                      {prop.name}
                    </NavLink>
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </React.Fragment>
      </Sider>
    );
  }
}
// Aside.propTypes = {
//   collapsed: PropTypes.bool
// };
const mapStateToProps = state => {
  return {
    collapsed: state.sideBar.collapsed
  };
};
export default connect(mapStateToProps, null)(withRouter(Aside));
