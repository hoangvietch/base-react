import React from 'react';
import { Layout, Icon, Avatar, Row, Col, Dropdown, Menu } from 'antd';
import PropTypes from 'prop-types';
import Aside from './sidebar';
import { connect } from 'react-redux';
import SideBarTypes from './../../redux/sidebar-redux';
import sidebarRoute from './../../routes/sidebar';
import AuthRoute from '../../routes/authRoute';
import './style.css';
const { Header, Content, Footer } = Layout;

class BasicLayout extends React.Component {
  render() {
    const { collapsed, isCollapsed } = this.props;
    const userInfo = (
      <Menu>
        <Menu.Item key="0">
          <a href="http://www.alipay.com/">Settings</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="1">
          <a href="http://www.taobao.com/">Profiles</a>
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="3">Logout</Menu.Item>
      </Menu>
    );
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Aside />
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Row style={{ padding: ' 0 15px' }}>
              <Col xs={4} sm={4} md={10} lg={10} xl={10}>
                <Icon
                  style={{ marginLeft: '20px' }}
                  className="trigger"
                  type={collapsed ? 'menu-unfold' : 'menu-fold'}
                  onClick={() => {
                    isCollapsed();
                  }}
                />
              </Col>
              <Col
                xs={20}
                sm={16}
                md={14}
                lg={14}
                xl={14}
                style={{ textAlign: 'right' }}
              >
                <Dropdown overlay={userInfo} trigger={['click']}>
                  <a className="ant-dropdown-link" href="#">
                    <Avatar
                      style={{ backgroundColor: '#87d068' }}
                      icon="user"
                    />
                  </a>
                </Dropdown>
              </Col>
            </Row>
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff'
            }}
          >
            {sidebarRoute.map((prop, key) => {
              if (prop.children && prop.children.length > 0) {
                return (
                  <div key={key}>
                    {prop.children.map((subItem, keyItem) => {
                      return (
                        <AuthRoute
                          exact
                          path={subItem.path}
                          component={subItem.component}
                          key={keyItem}
                        />
                      );
                    })}
                  </div>
                );
              } else {
                return (
                  <AuthRoute
                    exact
                    path={prop.path}
                    component={prop.component}
                    key={key}
                  />
                );
              }
            })}
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
BasicLayout.propTypes = {
  collapsed: PropTypes.bool,
  isCollapsed: PropTypes.func
};
const mapStateToProps = state => {
  return {
    collapsed: state.sideBar.collapsed
  };
};
const mapDispatchToProps = dispatch => {
  return {
    isCollapsed: () => {
      dispatch(SideBarTypes.getClickSideBar());
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BasicLayout);
