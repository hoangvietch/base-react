import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import BasicLayout from './../components/layout';
import Login from './../components/auth/Login';
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();

class App extends React.Component {
  render() {
    return (
      <div>
        <div className="App">
          <Router history={history}>
            <Switch>
              <Route path="/" exact component={Login} />
              <BasicLayout />
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}
export default App;
