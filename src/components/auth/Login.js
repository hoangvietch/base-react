/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Card, Avatar } from 'antd';
import './Login.css';
const FormItem = Form.Item;
class NormalLoginForm extends Component {
  // eslint-disable-next-line no-unused-vars
  checkUsername = (rule, value, callback) => {
    const form = this.props.form;
    form.setFields({
      username: {
        value: 'asdas'
      }
    });
    form.setFieldsValue('pedro, manada');
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="Login-page">
        <Row
          type="flex"
          justify="center"
          align="middle"
          style={{ minHeight: '100vh' }}
        >
          <Card style={{ textAlign: 'center' }}>
            <Avatar style={{ marginBottom: '20px' }} size="large" src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            <Form onSubmit={this.handleSubmit} className="login-form">
              <FormItem>
                {getFieldDecorator('userName', {
                  rules: [
                    { required: true, message: 'Please input your username!' },
                    { validator: this.checkUsername }
                  ]
                })(
                  <Input
                    prefix={<Icon type="user" style={{ fontSize: 13 }} />}
                    placeholder="Username"
                  />
                )}
              </FormItem>

              <FormItem>
                {getFieldDecorator('password', {
                  rules: [
                    { required: true, message: 'Please input your Password!' }
                  ]
                })(
                  <Input
                    prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
                    type="password"
                    placeholder="Password"
                  />
                )}
              </FormItem>

              <FormItem>
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })(<Checkbox>Remember me</Checkbox>)}
                <a className="login-form-forgot" href="">
                  Forgot password
                </a>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Log in
                </Button>
              </FormItem>
            </Form>
          </Card>
        </Row>
      </div>
    );
  }
}

const Login = Form.create()(NormalLoginForm);

export default Login;
