/* eslint-disable no-undef */
import { combineReducers } from 'redux';
import { reducer as modal } from 'redux-modal';

const rootReducer = combineReducers({
  modal,
  sideBar: require('./sidebar-redux').reducer
  
});

export default rootReducer;
